# Visions Unite

This is the configuration code for the [Visions Unite](https://pwgd.org)
web site. The friendly face of People Who Give A Damn.

Test URL: https://visionsunite-test.drutopia.org
Live URL: https://visionsunite.org

## Get submodules

This project includes the [PWGD styleguide](https://gitlab.com/pwgd/patternlibrary) as a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

It must be deployed by Drutopia_Hosting. See the hosting_private repo.
### When cloning

```
git clone --recurse-submodules git@gitlab.com:drutopia-platform/sites/visions-unite.git
```

### If you cloned without `--recurse-submodules`

If in your initial clone you didn't do as above, you can get the styleguide with:

```
git submodule init
git submodule update
```

### Get started

To be able to import the configuration, you need to get the initializing database (where we will also be collaborating on content) [from the test site](https://visionsunite.drutopia.org/) pending resolution of [drutopia#216](https://gitlab.com/drutopia/drutopia/issues/216).

```
vagrant ssh
composer install
drush -y sql-dump > /tmp/paranoia.sql && drush sql-drop && drush -y sql-sync @live @self && drush -y updb
drush -y rsync @live:%files @self:%files
```

### Get updates

```
git pull
git submodule update --recursive --remote
vagrant ssh
composer install
drush -y sql-dump > /tmp/paranoia.sql && drush -y sql-drop && drush -y sql-sync @live @self && drush -y updb
drush -y rsync @live:%files @self:%files
drush cim -y
```

## Theming

See `web/themes/custom/agarica/README.md` and `web/themes/custom/agarica/patternlibrary/README.md`

## Deployment

Visions Unite is currently using Drutopia's software as a service.  If necessary it will switch to the Platform as a Service version of Drutopia which allows additional modules.

Set up [drutopia_host](https://gitlab.com/drutopia-platform/drutopia_host) and [hosting_private](https://gitlab.com/drutopia-platform/hosting_private), as documented in hosting private.

Then use [ahoy](https://github.com/ahoy-cli/ahoy/), from within the hosting_private directory.

Ensure all three related repositories are up-to-date with:

```
ahoy git-pull-all
```

If Drutopia's base has changed (the composer.lock in build_source), produce and push a new build:

```
ahoy deploy-build stable
```

To deploy everything else (config, templates, styles):

```
ahoy deploy-site visionsunite_live
```

Then record the deployment. This applies to both deploy-site and deploy-build record keeping:
Navigate to `/drutopia_host/build/artifacts`
Add all new files generated with `git add . `
Commit the files with a message on what you deployed with  'git commit -m '
Push the changes to the repo with `git push`

If you need to overwrite live configuration (only after confirming you've committed any parts you want to keep) you can use ahoy for that too with `deploy-site-force`.
