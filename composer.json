{
    "name": "drutopia/drutopia_template",
    "description": "Project template for Drupal 8 Drutopia projects with a relocated document root",
    "type": "project",
    "license": "GPL-2.0-or-later",
    "minimum-stability": "dev",
    "prefer-stable": true,
    "homepage": "https://gitlab.com/drutopia",
    "support": {
        "docs": "http://docs.drutopia.org/en/latest"
    },
    "repositories": {
        "drupal/octavia_camouflage": {
            "type": "git",
            "url": "git@gitlab.com:drutopia/octavia_camouflage.git"
        },
        "drupal/drutopia_collection": {
            "type": "git",
            "url": "git@gitlab.com:drutopia/drutopia_collection.git"
        },
        "drutopia": {
            "type": "vcs",
            "url": "https://gitlab.com/drutopia/drutopia"
        },
        "drupal/drutopia_core": {
            "type": "git",
            "url": "git@gitlab.com:drutopia/drutopia_core.git"
        },
        "drupal": {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        }
    },
    "require": {
        "composer/installers": "^1.9",
        "cweagans/composer-patches": "^1.7",
        "drupal/allowed_formats": "^1.1",
        "drupal/antibot": "^1.2",
        "drupal/backup_migrate": "^5",
        "drupal/block_class": "^1.3",
        "drupal/ckeditorheight": "^1.3",
        "drupal/classitup": "^1",
        "drupal/convert_bundles": "^1.0@alpha",
        "drupal/core-composer-scaffold": "^9",
        "drupal/core-recommended": "^9",
        "drupal/csp": "^1.13",
        "drupal/diff": "^1.0",
        "drupal/drd_agent": "^4",
        "drupal/drutopia_article": "1.x-dev@dev",
        "drupal/drutopia_blog": "1.x-dev@dev",
        "drupal/drutopia_collection": "dev-8.x-1.x as 1.x-dev",
        "drupal/drutopia_core": "dev-8.x-1.x as 1.x-dev",
        "drupal/drutopia_event": "1.x-dev@dev",
        "drupal/drutopia_paragraph_title": "1.x-dev",
        "drupal/drutopia_resource": "1.x-dev@dev",
        "drupal/easy_breadcrumb": "^2.0",
        "drupal/editor_advanced_link": "^2.0",
        "drupal/editoria11y": "^1.0",
        "drupal/empty_page": "^3",
        "drupal/entity_reference_override": "^2",
        "drupal/environment_indicator": "^4.0",
        "drupal/exif_orientation": "^1.1",
        "drupal/field_defaults": "^1.3",
        "drupal/field_formatter_class": "^1.5",
        "drupal/field_token_value": "^2",
        "drupal/fitvids": "^1.1",
        "drupal/fixed_block_content": "^1.0",
        "drupal/footnotes": "2.x-dev",
        "drupal/geolocation": "^3.2",
        "drupal/give": "2.0.x-dev@dev",
        "drupal/indieweb": "^1.12",
        "drupal/insert": "^2.0",
        "drupal/key": "^1.14",
        "drupal/link_attributes": "^1.9",
        "drupal/linkit": "^6",
        "drupal/markdown": "^3",
        "drupal/menu_link_config": "^1.0@alpha",
        "drupal/menu_trail_by_path": "^1.3",
        "drupal/microformats": "^2",
        "drupal/migrate_plus": "^5",
        "drupal/migrate_source_csv": "^3",
        "drupal/migrate_tools": "^4.1",
        "drupal/migrate_upgrade": "^3",
        "drupal/minimalhtml": "^1.1",
        "drupal/node_view_permissions": "^1.4",
        "drupal/noreferrer": "^1.7",
        "drupal/notfoundpassthrough": "1.x-dev",
        "drupal/octavia": "1.x-dev",
        "drupal/octavia_camouflage": "dev-8.x-1.x as 1.x-dev",
        "drupal/password_policy": "^3.0",
        "drupal/plausible": "^2.0@beta",
        "drupal/preview_link": "^1.7",
        "drupal/role_delegation": "^1.0",
        "drupal/schema_metatag": "^2.2",
        "drupal/scn": "^1.1",
        "drupal/search404": "^2.0",
        "drupal/search_api_saved_searches": "^1.0@alpha",
        "drupal/seckit": "^2.0",
        "drupal/shield": "^1.4",
        "drupal/subpathauto": "^1.0@RC",
        "drupal/taxonomy_manager": "^2",
        "drupal/textarea_widget_for_text": "^1.1",
        "drupal/trim": "^1.0",
        "drupal/twigsuggest": "^1",
        "drupal/urllogin": "^2",
        "drupal/view_unpublished": "^1.0",
        "drupal/webform": "^6.0",
        "drupal/weight": "^3.3",
        "drupal/wysiwyg_linebreaks": "^1.10",
        "drush/drush": "^10",
        "drutopia/drutopia": "dev-8.x-1.x",
        "league/commonmark": "^1.0",
        "vlucas/phpdotenv": "^5.1",
        "webflo/drupal-finder": "^1.2"
    },
    "require-dev": {
        "behat/behat": "^3.4",
        "behat/mink": "^1.7",
        "behat/mink-goutte-driver": "^1.2",
        "drupal/upgrade_status": "^3"
    },
    "config": {
        "sort-packages": true,
        "process-timeout": 600,
        "allow-plugins": {
            "cweagans/composer-patches": true,
            "composer/installers": true,
            "drupal/core-composer-scaffold": true,
            "drupal/core-project-message": false
        }
    },
    "conflict": {
        "drupal/drupal": "*"
    },
    "extra": {
        "drupal-scaffold": {
            "locations": {
                "web-root": "web/"
            },
            "allowed-packages": [
                "drupal/ui_patterns"
            ]
        },
        "installer-paths": {
            "web/core": ["type:drupal-core"],
            "web/libraries/{$name}": ["type:drupal-library"],
            "web/modules/contrib/{$name}": ["type:drupal-module"],
            "web/profiles/contrib/{$name}": ["type:drupal-profile"],
            "web/themes/contrib/{$name}": ["type:drupal-theme"],
            "drush/Commands/contrib/{$name}": ["type:drupal-drush"],
            "web/modules/custom/{$name}": ["type:drupal-custom-module"],
            "web/themes/custom/{$name}": ["type:drupal-custom-theme"]
        },
        "merge-plugin": {
            "include": [
                "web/modules/contrib/webform/composer.libraries.json"
            ]
        },
        "enable-patching": true,
        "patches": {
            "Parent theme template inheritance bug": "https://www.drupal.org/files/issues/2021-02-22/2895316-12.patch"
        }
    },
    "scripts": {
        "nuke": "rm -rf vendor web/core web/modules/contrib web/profiles/contrib drush/contrib",
        "pull": "scripts/pull.sh",
        "quick-start": [
            "composer install",
            "php docroot/core/scripts/drupal quick-start drutopia --no-interaction"
        ]
    }
}
