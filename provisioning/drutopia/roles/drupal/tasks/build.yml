---
# If re-enabled, allow to fail when missing:
# - name: reclaim permission to settings.local.php
#   file:
#     path: "{{ build_dir }}/web/sites/default/settings.local.php"
#     state: file
#     mode: "0766"
#   delegate_to: 127.0.0.1
#   run_once: true
#
# - name: reclaim permission to settings folder
#   file:
#     path: "{{ build_dir }}/web/sites/default"
#     state: directory
#     mode: "0777"
#     recurse: true
#   delegate_to: 127.0.0.1
#   run_once: true

- name: delete build directory
  file:
    path: "{{ build_dir }}"
    state: absent
  delegate_to: 127.0.0.1
  run_once: true

- name: re-create build directory
  file:
    path: "{{ build_dir }}"
    state: directory
  delegate_to: 127.0.0.1
  run_once: true

- name: checkout version {{ git_branch }}
  git:
    accept_hostkey: yes
    clone: yes
    depth: 1
    dest: "{{ build_dir }}"
    name: "{{ git_repo }}"
    update: yes
    version: "{{ git_branch }}"
  delegate_to: 127.0.0.1
  run_once: true

- name: set site directory permissions
  file:
    path: "{{ build_dir }}"
    state: directory
    mode: "0755"
  delegate_to: 127.0.0.1
  run_once: true

# - name: generate css
# Need correct theme path? Just use theme_name as shown?:
#   command: sassc {{ build_dir }}/web/themes/custom/{{ theme_name }}/sass/styles.scss {{ build_dir }}/web/themes/custom/{{ theme_name }}/css/styles.css
#   delegate_to: 127.0.0.1
#   run_once: true

- name: create settings.local.php
  template:
    src: "settings.local.php.j2"
    dest: "{{ build_dir }}/web/sites/default/settings.local.php"
    mode: "0755"
  delegate_to: 127.0.0.1

- name: install composer-managed items
  composer:
    command: install
    no_dev: true
    optimize_autoloader: true
    working_dir: "{{ build_dir }}"
  delegate_to: 127.0.0.1
  run_once: true
